import { render } from 'react-dom';
import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { Router, Route, browserHistory } from 'react-router';
import rootReducer from './reducers';
import { loadCharacterPage } from './actions';
import App from './components/app';

const store = createStore(
  rootReducer,
  applyMiddleware(thunk)
);

const ensureLoad = ({ params: { character, page } }) => {
  store.dispatch(loadCharacterPage(character, Number(page || 1)));
};

render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/:character(/:page)" component={App} onEnter={ensureLoad} />
      <Route path="/" component={App} />
    </Router>
  </Provider>,
  document.getElementById('main')
);
