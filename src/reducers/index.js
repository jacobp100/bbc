import { set } from 'lodash/fp';

const defaultState = {
  pageCountPerCharacter: {},
  pagesPerCharacter: {},
};

const store = (state = defaultState, action) => {
  switch (action.type) {
    case 'SET_PAGE_COUNT_PER_CHARACTER':
      return set(['pageCountPerCharacter', action.character], action.pageCount, state);
    case 'SET_PAGE':
      return set(['pagesPerCharacter', action.character, action.page], action.elements, state);
    default:
      return state;
  }
};

export default store;
