import { range, map } from 'lodash/fp';
import request from 'superagent';

const PRELOAD_N_FUTURE_PAGES = 1;

const loadLetterPage = (inputCharacter, inputPage, isPreload = false) => (dispatch) => (
  new Promise((resolve, reject) => {
    request
      .get(`https://ibl.api.bbci.co.uk/ibl/v1/atoz/${inputCharacter}/programmes?page=${inputPage}`)
      .end((err, res) => {
        if (err) {
          if (isPreload) {
            // FIXME: dispatch an error
          }
          reject(err);
        } else {
          const { character, page, count, per_page: perPage, elements } = res.body.atoz_programmes;
          const pageCount = Math.ceil(count / perPage);
          dispatch({ type: 'SET_PAGE_COUNT_PER_CHARACTER', character, pageCount });
          dispatch({ type: 'SET_PAGE', character, page, elements });
          resolve({ character, page, elements, pageCount, isPreload });
        }
      });
  })
);

export const loadCharacterPage = (character, page) => (dispatch) => (
  dispatch(loadLetterPage(character, page))
    .then(({ pageCount }) => {
      const pagesToPreload = range(
        Math.min(page, pageCount) + 1,
        Math.min(page + PRELOAD_N_FUTURE_PAGES, pageCount) + 1
      );
      const preloadPages = map(nextPage => (
        dispatch(loadLetterPage(character, nextPage, true))
      ), pagesToPreload);
      return Promise.all(preloadPages);
    })
);
