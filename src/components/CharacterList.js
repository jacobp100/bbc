import { range, map } from 'lodash/fp';
import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import classnames from 'classnames';
import * as characterList from '../../styles/character-list.css';

const aToZ = map(
  charCode => String.fromCharCode(charCode),
  range('a'.charCodeAt(0), 'z'.charCodeAt(0) + 1)
);

const CharacterList = ({ character: currentCharacter }) => (
  <div className={characterList.container}>
    {
      map(character => (
        <Link
          key={character}
          className={classnames(
            characterList.button,
            { [characterList.buttonActive]: currentCharacter === character }
          )}
          to={`/${character}/1`}
        >
          { character }
        </Link>
      ), aToZ)
    }
  </div>
);

CharacterList.propTypes = {
  character: PropTypes.string,
};

export default CharacterList;
