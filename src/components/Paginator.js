import { range, map } from 'lodash/fp';
import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import classnames from 'classnames';
import * as paginator from '../../styles/paginator.css';


const Paginator = ({ character, page, pageCount }) => (
  <div className={paginator.container}>
    {
      map(index => (
        index !== page
          ? (
            <Link key={index} className={paginator.button} to={`/${character}/${index}`}>
              { index }
            </Link>
          ) : (
            <span key={index} className={classnames(paginator.button, paginator.buttonActive)}>
              { index }
            </span>
          )
      ), range(1, pageCount || 1))
    }
  </div>
);

Paginator.propTypes = {
  character: PropTypes.string,
  page: PropTypes.number,
  pageCount: PropTypes.number,
};

export default Paginator;
