import { map } from 'lodash/fp';
import React, { PropTypes } from 'react';
import Listing from './Listing';
import * as page from '../../styles/page.css';

const Page = ({ elements }) => {
  let titles;

  if (elements) {
    titles = map(({ title, images }) => (
      <Listing key={title} title={title} image={images.standard} width={406} height={228} />
    ), elements);
  } else {
    titles = <div className={page.loading}>Loading&hellip;</div>;
  }

  return (
    <div>
      { titles }
    </div>
  );
};

Page.propTypes = {
  elements: PropTypes.array,
};

export default Page;
