import { identity, get } from 'lodash/fp';
import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import CharacterList from './CharacterList';
import Page from './Page';
import Paginator from './Paginator';

const App = ({
  pageCountPerCharacter,
  pagesPerCharacter,
  params: {
    character,
    page,
  },
}) => (
  <div>
    <CharacterList character={character} />
    <Page elements={get([character, page], pagesPerCharacter)} />
    {
      pageCountPerCharacter && <Paginator
        character={character}
        page={Number(page)}
        pageCount={get(character, pageCountPerCharacter)}
      />
    }
  </div>
);

App.propTypes = {
  params: PropTypes.object,
  pageCountPerCharacter: PropTypes.object,
  pagesPerCharacter: PropTypes.object,
};

export default connect(identity)(App);
