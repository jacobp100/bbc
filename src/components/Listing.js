import React, { PropTypes } from 'react';
import * as listing from '../../styles/listing.css';

const Listing = ({ title, image, width, height }) => (
  <div className={listing.container} style={{ width }}>
    <img
      className={listing.image}
      src={image.replace('{recipe}', `${width}x${height}`)}
      width={width}
      height={height}
      alt="preview"
    />
    <div className={listing.titleContainer}>
      <span className={listing.title}>{ title }</span>
    </div>
  </div>
);

Listing.propTypes = {
  title: PropTypes.string,
  image: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

export default Listing;
