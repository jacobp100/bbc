/* eslint-env mocha */

import { stub } from 'sinon';
import request from 'superagent';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { loadCharacterPage } from '../src/actions';
import { assert } from 'chai';

const mockStore = configureMockStore([thunk]);

const generateHandler = (err, body) => ({
  end: (fn) => fn(err, body && {
    body: {
      atoz_programmes: body,
    },
  }),
});

describe('actions', () => {
  describe('loadCharacterPage', () => {
    beforeEach(() => {
      stub(request, 'get');
    });

    afterEach(() => {
      request.get.restore();
    });

    it('should request a url (no get.end specified)', (done) => {
      const expectedActions = [];
      const store = mockStore({}, expectedActions);

      store.dispatch(loadCharacterPage('a', 1))
        .catch(() => {
          assert.deepEqual(store.getActions(), []);
          assert.equal(request.get.callCount, 1);
          assert.deepEqual(request.get.firstCall.args, [
            'https://ibl.api.bbci.co.uk/ibl/v1/atoz/a/programmes?page=1',
          ]);
          done();
        });
    });

    it('should not dispatch anything when there is an error', (done) => {
      request.get.returns(generateHandler('Oh no! An error.'));

      const store = mockStore({});
      store.dispatch(loadCharacterPage('a', 1))
        .catch(() => {
          assert.deepEqual(store.getActions(), []);
          assert.equal(request.get.callCount, 1);
          assert.deepEqual(request.get.firstCall.args, [
            'https://ibl.api.bbci.co.uk/ibl/v1/atoz/a/programmes?page=1',
          ]);
          done();
        });
    });

    it('should dispatch on a proper body', (done) => {
      request.get.returns(generateHandler(null, {
        character: 'a',
        page: 1,
        count: 1,
        per_page: 20,
        elements: [],
      }));

      const store = mockStore({});

      store.dispatch(loadCharacterPage('a', 1))
        .then(() => {
          assert.deepEqual(store.getActions(), [
            { type: 'SET_PAGE_COUNT_PER_CHARACTER', character: 'a', pageCount: 1 },
            { type: 'SET_PAGE', character: 'a', page: 1, elements: [] },
          ]);
          assert.equal(request.get.callCount, 1);
          assert.deepEqual(request.get.firstCall.args, [
            'https://ibl.api.bbci.co.uk/ibl/v1/atoz/a/programmes?page=1',
          ]);
          done();
        })
        .catch(done);
    });

    it('should preload the next page', (done) => {
      request.get
        .onFirstCall().returns(generateHandler(null, {
          character: 'a',
          page: 1,
          count: 30,
          per_page: 20,
          elements: ['a', 'b'],
        }))
        .onSecondCall().returns(generateHandler(null, {
          character: 'a',
          page: 2,
          count: 30,
          per_page: 20,
          elements: ['c'],
        }));

      const store = mockStore({});

      store.dispatch(loadCharacterPage('a', 1))
        .then(() => {
          assert.deepEqual(store.getActions(), [
            { type: 'SET_PAGE_COUNT_PER_CHARACTER', character: 'a', pageCount: 2 },
            { type: 'SET_PAGE', character: 'a', page: 1, elements: ['a', 'b'] },
            { type: 'SET_PAGE_COUNT_PER_CHARACTER', character: 'a', pageCount: 2 },
            { type: 'SET_PAGE', character: 'a', page: 2, elements: ['c'] },
          ]);
          assert.equal(request.get.callCount, 2);
          assert.deepEqual(request.get.firstCall.args, [
            'https://ibl.api.bbci.co.uk/ibl/v1/atoz/a/programmes?page=1',
          ]);
          assert.deepEqual(request.get.secondCall.args, [
            'https://ibl.api.bbci.co.uk/ibl/v1/atoz/a/programmes?page=2',
          ]);
          done();
        })
        .catch(done);
    });
  });
});
